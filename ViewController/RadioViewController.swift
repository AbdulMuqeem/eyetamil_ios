//
//  RadioViewController.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 31/01/2021.
//

import UIKit
import SwiftAudio
import MediaPlayer

class RadioViewController: UIViewController {

    class func instantiateFromStoryboard() -> RadioViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RadioViewController
    }
    
    @IBOutlet weak var btnPlay:UIButton!
    var isPlay:Bool! = false
    
    // Audio player object to play stream
    let audioPlayer = AudioPlayer()
    let audioUrl = "https://streams.radio.co/s937ac5492/listen"
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LightStatusBar()
        self.navigationController?.HideNavigationBar()
        
        //Audio Player
        self.setupAudioPlayer()
        
    }

    @IBAction func playAction(_ sender : UIButton) {
        
        if self.isPlay == false {
            self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_pause_btn_orange"), for: .normal)
            self.isPlay = true
            self.playStream()
        }
        else {
            self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_orange"), for: .normal)
            self.isPlay = false
            self.audioPlayer.stop()
        }
        
    }

}

// MARK: Audio player
extension RadioViewController {
    
    func setupAudioPlayer() {
        
        audioPlayer.automaticallyWaitsToMinimizeStalling = true
        
        audioPlayer.automaticallyUpdateNowPlayingInfo = true
        
        audioPlayer.nowPlayingInfoController.set(keyValue: NowPlayingInfoProperty.isLiveStream(true))
        
        audioPlayer.nowPlayingInfoController.set(keyValue: NowPlayingInfoProperty.playbackRate(1.0))
        
        audioPlayer.remoteCommands = [.play, .stop , .pause, .togglePlayPause]
        
        audioPlayer.event.stateChange.addListener(self, handleAudioPlayerStateChange(state:))
        
        audioPlayer.remoteCommandController.handlePlayCommand = self.handlePlayCommand
        
    }
    
    private func handlePlayCommand(event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
        self.playStream()
        return MPRemoteCommandHandlerStatus.success
    }
    
    func handleAudioPlayerStateChange(state: AudioPlayerState) {
        
        switch state {
        
        case .buffering:
            print("Buffering...")
//            self.startLoading(message: "")

        case .loading:
            print("Loading stream")
//            self.startLoading(message: "")
            
        case .ready:
            print("Loaded stream")
            
            try? AudioSessionController.shared.set(category: .playback)
            
            // stream is ready to play, activate audio session
            try? AudioSessionController.shared.activateSession()
            
            // disable intrruption notifications
            AudioSessionController.shared.isObservingForInterruptions = false
            
        case .paused:
            print("Stream Paused")
//            self.stopLoading()
        
        case .playing:
            print("Stream Playing")
//            self.stopLoading()
        
        case .idle:
            print("Player is idle")
            
        }
        
    }
    
    func playStream() {
        
        // create audio item with stream URL
        let audioItem = DefaultAudioItem(audioUrl: self.audioUrl , artist: "Online Stream", title: "", albumTitle: "", sourceType: .stream , artwork: UIImage())
        
        // try to play the stream
        try? audioPlayer.load(item: audioItem,playWhenReady: true)
        
    }
    
}


//MARK:- Tab Bar Actions
extension RadioViewController {
    
    @IBAction func homeAction(_ sender : UIButton) {
        
        self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
        self.isPlay = false
        self.audioPlayer.stop()
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @IBAction func newsAction(_ sender : UIButton) {
        
        self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
        self.isPlay = false
        self.audioPlayer.stop()
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: NewsViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else {
                let vc = NewsViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func eyetubeAction(_ sender : UIButton) {
        
        self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
        self.isPlay = false
        self.audioPlayer.stop()
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: EyeTubeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else {
                let vc = EyeTubeViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
        }
    }
    
}
