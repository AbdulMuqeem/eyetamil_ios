//
//  EyeTubeViewController.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 31/01/2021.
//

import UIKit

class EyeTubeViewController: UIViewController {

    class func instantiateFromStoryboard() -> EyeTubeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EyeTubeViewController
    }
    
    @IBOutlet weak var collectionView:UICollectionView!
    var eyeTubeListArray:[EyeTubeData]? = [EyeTubeData]()
    
    var page = 1
    var perPage = 10

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LightStatusBar()
        self.navigationController?.HideNavigationBar()
        
        self.getEyeTubeList()
        
        //register cell
        let nib = UINib(nibName: "HomeCollectionCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "HomeCollectionCell")
        
    }

    //MARK:- Get EyeTube List Api
    func getEyeTubeList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:WARNING)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["page":self.page , "per_page":self.perPage]
        print("Parameters : \(params)")
        
        Services.GetEyeTube(param: params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.showBanner(title: "Error", subTitle: ERROR_MESSAGE , type: WARNING)
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                self.showBanner(title: "Error", subTitle: msg! , type: WARNING)
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?.arrayValue
            for resultObj in responseArray! {
                let obj =  EyeTubeData(json: resultObj)
                self.eyeTubeListArray?.append(obj)
            }
            
            self.collectionView.reloadData()
            
        })
        
    }

}

extension EyeTubeViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.eyeTubeListArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = GISTUtility.convertToRatio(160.0)
        let screenWidth =  collectionView.bounds.size.width - 20
        return CGSize(width: screenWidth/2 , height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (indexPath.row == (self.eyeTubeListArray!.count) - 1) && ((self.eyeTubeListArray!.count) % 10) == 0 {
            print("Load more")
            
            self.page += 1
            self.perPage = self.perPage + 10
            self.getEyeTubeList()
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj = self.eyeTubeListArray![indexPath.row]
        
        let cell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeCollectionCell.self), for: indexPath) as! HomeCollectionCell
                
        if let image = obj.featured_image_src {
            cell.imgNews.setImageFromUrl(urlStr: image)
        }
        
        if let title = obj.title!.rendered {
            cell.lblHeading.text = title
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.eyeTubeListArray![indexPath.row]

        let vc = DetailViewController.instantiateFromStoryboard()
        vc.eyeTubeData = obj
        let navVc = UINavigationController(rootViewController: vc)
        navVc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navVc, animated: true, completion: nil)
        
    }
}


//MARK:- Tab Bar Actions
extension EyeTubeViewController {
    
    @IBAction func homeAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @IBAction func radioAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RadioViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else {
                let vc = RadioViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func newsAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: NewsViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else {
                let vc = NewsViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
        }
    }
    
}
