//
//  RootViewController.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 19/01/2021.
//

import UIKit

class RootViewController: UINavigationController {

    class func instantiateFromStoryboard() -> RootViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
