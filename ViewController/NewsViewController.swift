//
//  NewsViewController.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 31/01/2021.
//

import UIKit

class NewsViewController: UIViewController {

    class func instantiateFromStoryboard() -> NewsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NewsViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
        
    var eyeTamilValamListArray:[NewsData]? = [NewsData]()
    var eyeTamilSriListArray:[NewsData]? = [NewsData]()
    var eyeTamilEventListArray:[NewsData]? = [NewsData]()
    var eyeTamilQueListArray:[NewsData]? = [NewsData]()
    var eyeTamilWorldListArray:[NewsData]? = [NewsData]()
    var eyeTamilPopularListArray:[NewsData]? = [NewsData]()
    var eyeTamilTechListArray:[NewsData]? = [NewsData]()
    var eyeTamilCinemaListArray:[NewsData]? = [NewsData]()
    var eyeTamilSportListArray:[NewsData]? = [NewsData]()
    var eyeTamilCanadianListArray:[NewsData]? = [NewsData]()
    
    var eyeTamilListArray:[NewsData]? = [NewsData]()
    var arrayCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LightStatusBar()
        self.navigationController?.HideNavigationBar()
        
        self.getEyeTamilNewsList()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:HomeTableViewCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: HomeTableViewCell.self))

    }

    //MARK:- Get EyeTamil News List Api
    func getEyeTamilNewsList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:WARNING)
            self.stopLoading()
            return
        }
        
        Services.GetEyeTamil(param: [:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.showBanner(title: "Error", subTitle: ERROR_MESSAGE , type: WARNING)
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                self.showBanner(title: "Error", subTitle: msg! , type: WARNING)
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if response!["data"].count < 1 {
                self.showBanner(title: "Error", subTitle: ERROR_MESSAGE , type: WARNING)
                return
            }
            
            let obj = EyeTamilData(object:(response?["data"])!)

            if let value = obj.valam {
                self.arrayCount += 1
                self.eyeTamilValamListArray = value
            }
            
            if let value = obj.sri {
                self.arrayCount += 1
                self.eyeTamilSriListArray?.append(contentsOf: value)
            }
            
            if let value = obj.event {
                self.arrayCount += 1
                self.eyeTamilEventListArray?.append(contentsOf: value)
            }
            
            if let value = obj.que {
                self.arrayCount += 1
                self.eyeTamilQueListArray?.append(contentsOf: value)
            }

            if let value = obj.world {
                self.arrayCount += 1
                self.eyeTamilWorldListArray?.append(contentsOf: value)
            }
            
            if let value = obj.popular {
                self.arrayCount += 1
                self.eyeTamilPopularListArray?.append(contentsOf: value)
            }

            if let value = obj.tech {
                self.arrayCount += 1
                self.eyeTamilTechListArray?.append(contentsOf: value)
            }
            
            if let value = obj.cinema {
                self.arrayCount += 1
                self.eyeTamilCinemaListArray?.append(contentsOf: value)
            }

            if let value = obj.sport {
                self.arrayCount += 1
                self.eyeTamilSportListArray?.append(contentsOf: value)
            }
            
            if let value = obj.canadian {
                self.arrayCount += 1
                self.eyeTamilCanadianListArray?.append(contentsOf: value)
            }
            
            print("Array Count : \(self.arrayCount)")
            self.tblView.reloadData()
            
        })
        
    }

}

extension NewsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(200.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeTableViewCell.self)) as!  HomeTableViewCell
        
        if indexPath.row == 0 {
            cell.lblName.text = "Popular"
            cell.eyeTamilData = self.eyeTamilPopularListArray
        }
        else if indexPath.row == 1 {
            cell.lblName.text = "Canadian"
            cell.eyeTamilData = self.eyeTamilCanadianListArray
        }
        else if indexPath.row == 2 {
            cell.lblName.text = "Que"
            cell.eyeTamilData = self.eyeTamilQueListArray
        }
        else if indexPath.row == 3 {
            cell.lblName.text = "Sri"
            cell.eyeTamilData = self.eyeTamilSriListArray
        }
        else if indexPath.row == 4 {
            cell.lblName.text = "World"
            cell.eyeTamilData = self.eyeTamilWorldListArray
        }
        else if indexPath.row == 5 {
            cell.lblName.text = "Cinema"
            cell.eyeTamilData = self.eyeTamilCinemaListArray
        }
        else if indexPath.row == 6 {
            cell.lblName.text = "Tech"
            cell.eyeTamilData = self.eyeTamilTechListArray
        }
        else if indexPath.row == 7 {
            cell.lblName.text = "Valam"
            cell.eyeTamilData = self.eyeTamilValamListArray
        }
        else if indexPath.row == 8 {
            cell.lblName.text = "Sports"
            cell.eyeTamilData = self.eyeTamilSportListArray
        }
        else if indexPath.row == 9 {
            cell.lblName.text = "Event"
            cell.eyeTamilData = self.eyeTamilEventListArray
        }

        cell.isHome = false
        cell.navigationDelegate = self
        cell.collectionView.reloadData()
        cell.indexpath = indexPath
        
        return cell
    }
    
}

//Call Delegate Action
extension NewsViewController : NavigationDelegate {
    
    func didNavigateEyeTamil(obj: NewsData) {
        
        let vc = DetailViewController.instantiateFromStoryboard()
        vc.eyeTamilNewsData = obj
        let navVc = UINavigationController(rootViewController: vc)
        navVc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navVc, animated: true, completion: nil)

    }

    func didNavigateEyeTube(obj: EyeTubeData) {

    }
    
}

//MARK:- Tab Bar Actions
extension NewsViewController {
    
    @IBAction func homeAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @IBAction func radioAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RadioViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else {
                let vc = RadioViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func eyetubeAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: EyeTubeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else {
                let vc = EyeTubeViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
        }
    }
    
}
