//
//  DetailViewController.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 14/02/2021.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    class func instantiateFromStoryboard() -> DetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailViewController
    }
    
    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    
    @IBOutlet weak var videoView:UIView!
    @IBOutlet weak var heightVideoView:NSLayoutConstraint!
    var webPlayer: WKWebView!

    var eyeTamilNewsData:NewsData?
    var eyeTubeData:EyeTubeData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.LightStatusBar()
        self.initialSetup()
        
    }
    
    func initialSetup() {
        
        self.title = "Details"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
    
        if self.eyeTamilNewsData != nil {
            
            if let image = self.eyeTamilNewsData!.imageUrl {
                self.imgBanner.setImageFromUrl(urlStr: image)
            }
            
            if let title = self.eyeTamilNewsData!.title {
                self.lblTitle.text = title
            }
            
            if let description = self.eyeTamilNewsData!.body {
                self.lblDescription.text = description.html2String
            }

            if let link = self.eyeTamilNewsData!.youtube {
                var url = link.html2String
                url = url.replacingOccurrences(of: "\n", with: "")
                url = url + "?playsinline=1"
                self.setupWebViewPlayer(url:url)
            }
            else {
                self.heightVideoView.constant = 0
                self.videoView.isHidden = true
            }
            
        }
        
        if self.eyeTubeData != nil {
            
            if let image = self.eyeTubeData!.featured_image_src {
                self.imgBanner.setImageFromUrl(urlStr: image)
            }
            
            if let title = self.eyeTubeData?.title!.rendered {
                self.lblTitle.text = title
            }

            if let link = self.eyeTubeData?.excerpt!.rendered {
                var value = link.html2String
                value = value.replacingOccurrences(of: "\n", with: "")
                let id = getDecodedValue(url: value)
                let url = "https://www.youtube.com/embed/\(id)?playsinline=1"
                self.setupWebViewPlayer(url:url)
            }
            else {
                self.heightVideoView.constant = 0
                self.videoView.isHidden = true
            }
            
        }

    }

    @objc func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupWebViewPlayer(url:String) {
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true
        
        DispatchQueue.main.async {
            
            self.webPlayer = WKWebView(frame: self.videoView.bounds, configuration: webConfiguration)
            self.webPlayer.contentMode = .scaleAspectFit
            self.videoView.addSubview(self.webPlayer)
            
//            guard let videoURL = URL(string: "https://www.youtube.com/embed/YE7VzlLtp-4?playsinline=1") else { return }
            guard let videoURL = URL(string: url) else {
                self.heightVideoView.constant = 0
                self.videoView.isHidden = true
                return
            }
            let request = URLRequest(url: videoURL)
            self.webPlayer.load(request)
            
        }
    }
    
}
