//
//  HomeViewController.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 19/01/2021.
//

import UIKit
import SwiftAudio
import MediaPlayer

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    @IBOutlet weak var btnPlay:UIButton!
    @IBOutlet weak var tblView:UITableView!
    
    var eyeTamilPopularListArray:[NewsData]? = [NewsData]()
    var eyeTubeListArray:[EyeTubeData]? = [EyeTubeData]()
    
    // Audio player object to play stream
    let audioPlayer = AudioPlayer()
    let audioUrl = "https://streams.radio.co/s937ac5492/listen"
    
    var isPlay:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LightStatusBar()
        self.navigationController?.HideNavigationBar()
        
        //Audio Player
        self.setupAudioPlayer()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:HomeTableViewCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: HomeTableViewCell.self))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getEyeTamilNewsList()
        self.getEyeTubeList()
        
        
    }
    
    //MARK:- Get EyeTamil News List Api
    func getEyeTamilNewsList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:WARNING)
            self.stopLoading()
            return
        }
        
        Services.GetEyeTamil(param: [:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.showBanner(title: "Error", subTitle: ERROR_MESSAGE , type: WARNING)
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                self.showBanner(title: "Error", subTitle: msg! , type: WARNING)
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if response!["data"].count < 1 {
                self.showBanner(title: "Error", subTitle: ERROR_MESSAGE , type: WARNING)
                return
            }
            
            let obj = EyeTamilData(object:(response?["data"])!)
            
            if let popular = obj.popular {
                self.eyeTamilPopularListArray = popular
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
    //MARK:- Get EyeTube List Api
    func getEyeTubeList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:WARNING)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["page":1 , "per_page":5]
        print("Parameters : \(params)")
        
        Services.GetEyeTube(param: params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.showBanner(title: "Error", subTitle: ERROR_MESSAGE , type: WARNING)
                    self.stopLoading()
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                self.showBanner(title: "Error", subTitle: msg! , type: WARNING)
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?.arrayValue
            for resultObj in responseArray! {
                let obj =  EyeTubeData(json: resultObj)
                self.eyeTubeListArray?.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
    
    @IBAction func playAction(_ sender : UIButton) {
        
        if self.isPlay == false {
            self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_pause_btn"), for: .normal)
            self.isPlay = true
            self.playStream()
        }
        else {
            self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
            self.isPlay = false
            self.audioPlayer.stop()
        }
        
    }
    
}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(200.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeTableViewCell.self)) as!  HomeTableViewCell
        
        if indexPath.row == 0 {
            cell.lblName.text = "Eyetamil News"
            cell.eyeTamilPopularListArray = self.eyeTamilPopularListArray
        }
        else if indexPath.row == 1 {
            cell.lblName.text = "Eyetube"
            cell.eyeTubeListArray = self.eyeTubeListArray
        }
        
        cell.isHome = true
        cell.navigationDelegate = self
        cell.collectionView.reloadData()
        cell.indexpath = indexPath
        
        return cell
    }
    
}

//Call Delegate Action
extension HomeViewController : NavigationDelegate {
    
    func didNavigateEyeTube(obj: EyeTubeData) {
        
        let vc = DetailViewController.instantiateFromStoryboard()
        vc.eyeTubeData = obj
        let navVc = UINavigationController(rootViewController: vc)
        navVc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navVc, animated: true, completion: nil)
        
    }
    
    func didNavigateEyeTamil(obj: NewsData) {
        
        let vc = DetailViewController.instantiateFromStoryboard()
        vc.eyeTamilNewsData = obj
        let navVc = UINavigationController(rootViewController: vc)
        navVc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navVc, animated: true, completion: nil)
        
    }
    
}

// MARK: Audio player
extension HomeViewController {
    
    func setupAudioPlayer() {
        
        audioPlayer.automaticallyWaitsToMinimizeStalling = true
        
        audioPlayer.automaticallyUpdateNowPlayingInfo = true
        
        audioPlayer.nowPlayingInfoController.set(keyValue: NowPlayingInfoProperty.isLiveStream(true))
        
        audioPlayer.nowPlayingInfoController.set(keyValue: NowPlayingInfoProperty.playbackRate(1.0))
        
        audioPlayer.remoteCommands = [.play, .stop , .pause, .togglePlayPause]
        
        audioPlayer.event.stateChange.addListener(self, handleAudioPlayerStateChange(state:))
        
        audioPlayer.remoteCommandController.handlePlayCommand = self.handlePlayCommand
        
    }
    
    private func handlePlayCommand(event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
        self.playStream()
        return MPRemoteCommandHandlerStatus.success
    }
    
    func handleAudioPlayerStateChange(state: AudioPlayerState) {
        
        switch state {
        
        case .buffering:
            print("Buffering...")
            
        case .loading:
            print("Loading stream")
            
        case .ready:
            print("Loaded stream")
            
            try? AudioSessionController.shared.set(category: .playback)
            
            // stream is ready to play, activate audio session
            try? AudioSessionController.shared.activateSession()
            
            // disable intrruption notifications
            AudioSessionController.shared.isObservingForInterruptions = false
            
        case .paused:
            print("Stream Paused")
            
        case .playing:
            print("Stream Playing")
            
        case .idle:
            print("Player is idle")
            
        }
        
    }
    
    func playStream() {
        
        // create audio item with stream URL
        let audioItem = DefaultAudioItem(audioUrl: self.audioUrl , artist: "Online Stream", title: "", albumTitle: "", sourceType: .stream , artwork: UIImage())
        
        // try to play the stream
        try? audioPlayer.load(item: audioItem,playWhenReady: true)
        
    }
    
}


//MARK:- Tab Bar Actions
extension HomeViewController {
    
    @IBAction func radioAction(_ sender : UIButton) {
        self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
        self.isPlay = false
        self.audioPlayer.stop()
        let vc = RadioViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func newsAction(_ sender : UIButton) {
        self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
        self.isPlay = false
        self.audioPlayer.stop()
        let vc = NewsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func eyetubeAction(_ sender : UIButton) {
        self.btnPlay.setImage(UIImage(imageLiteralResourceName: "bg_play_btn"), for: .normal)
        self.isPlay = false
        self.audioPlayer.stop()
        let vc = EyeTubeViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
