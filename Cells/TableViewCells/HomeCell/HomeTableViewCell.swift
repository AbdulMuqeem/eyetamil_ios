//
//  HelpTableViewCell.swift
//  Paysii
//
//  Created by Abdul Muqeem on 21/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var collectionView:UICollectionView!
    
    var eyeTamilPopularListArray:[NewsData]? = [NewsData]()
    var eyeTubeListArray:[EyeTubeData]? = [EyeTubeData]()
    var eyeTamilData:[NewsData]? = [NewsData]()
    
    var indexpath:IndexPath?
    var navigationDelegate: NavigationDelegate?
    
    var isHome = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //register cell
        let nib = UINib(nibName: "HomeCollectionCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "HomeCollectionCell")
        
    }
    
}

extension HomeTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.isHome == true {
            if self.indexpath!.row == 0 {
                return self.eyeTamilPopularListArray!.count
            }
            return self.eyeTubeListArray!.count
        }

        return self.eyeTamilData!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = GISTUtility.convertToRatio(160.0)
        let screenWidth =  collectionView.bounds.size.width - 20
        return CGSize(width: screenWidth/1.9 , height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeCollectionCell.self), for: indexPath) as! HomeCollectionCell
        
        if self.isHome == true {
            
            if self.indexpath!.row == 0 {
                
                let obj = self.eyeTamilPopularListArray![indexPath.row]
                
                if let image = obj.imageUrl {
                    cell.imgNews.setImageFromUrl(urlStr: image)
                }
                
                if let title = obj.title {
                    cell.lblHeading.text = title
                }
                
            }
            else if indexpath!.row == 1 {
                
                let obj = self.eyeTubeListArray![indexPath.row]
                
                if let image = obj.featured_image_src {
                    cell.imgNews.setImageFromUrl(urlStr: image)
                }
                
                if let title = obj.title!.rendered {
                    cell.lblHeading.text = title
                }

            }
        }
        else {
            
            let obj = self.eyeTamilData![indexPath.row]
            
            if let image = obj.imageUrl {
                cell.imgNews.setImageFromUrl(urlStr: image)
            }
            
            if let title = obj.title {
                cell.lblHeading.text = title
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.isHome == true {
            if self.indexpath!.row == 0 {
                let obj = self.eyeTamilPopularListArray![indexPath.row]
                self.navigationDelegate?.didNavigateEyeTamil(obj: obj)
            }
            else if indexpath!.row == 1 {
                let obj = self.eyeTubeListArray![indexPath.row]
                self.navigationDelegate?.didNavigateEyeTube(obj: obj)
            }
        }
        else {
            let obj = self.eyeTamilData![indexPath.row]
            self.navigationDelegate?.didNavigateEyeTamil(obj: obj)
        }
        
    }
}
