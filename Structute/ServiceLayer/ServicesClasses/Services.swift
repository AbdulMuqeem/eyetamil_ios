//
//  UserServices.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class Services {
    
    static func GetEyeTamil(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.eyeTamil , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
                  
            if response!["data"].count < 1 {
                completionHandler(false, response!, nil)
                return
            }
            
            if response!["message"].stringValue == "Success " || response!["message"].stringValue == "Success" {
                completionHandler(true, response!, nil)
                return
            }
            completionHandler(false, response!, nil)
        })
    }
    
    static func GetEyeTube(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.eyeTube , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response!.count < 1 || response!["data"]["status"].intValue == 400 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    
}
