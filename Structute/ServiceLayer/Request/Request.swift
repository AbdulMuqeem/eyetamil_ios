//
//  Request.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class Request: NSObject {
    
    static func GetRequestWithHeader(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Int? , Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
        let headers:[String:String] = ["Authorization" : "Bearer "+token+""]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .get, parameters: parameters , headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let code = response.response!.statusCode
                        let response = JSON(value)
                        callback?(response, code , nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, nil , response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, nil , response.result.error!)
                break
            }
        }
    }
    
    static func GetRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .get, parameters: parameters , headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequestWithHeader(fromSavedUrl url: String , parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
        let headers:[String:String] = ["Authorization" : "Bearer "+token+""]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: JSONEncoding.prettyPrinted , headers: headers).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: JSONEncoding.prettyPrinted , headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        
                        if let ALLheader = response.response?.allHeaderFields  {
                            if let header = ALLheader as? [String : Any] {
                                if let cookies = header["Set-Cookie"] as? String {
                                    let token = cookies.slice(from: "token=", to: ";")
                                    UserDefaults.standard.set(token, forKey: LOGIN_TOKEN)
                                }
                            }
                        }

                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequestwithQuery(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
        let headers:[String:String] = ["Authorization" : "Bearer "+token+""]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: URLEncoding.queryString, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PutRequestWithHeader(fromSavedUrl url: String , parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
        let headers:[String:String] = ["Authorization" : "Bearer "+token+""]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .put , parameters: parameters ,encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
            
        }
    }
    
    static func DeleteRequestWithHeader(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        let token = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
        let headers:[String:String] = ["Authorization" : "Bearer "+token+""]
        print("Header: \(headers)")
        
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest,
                let headers = originalRequest.allHTTPHeaderFields,
                let authorizationHeaderValue = headers["Authorization"]
            {
                var mutableRequest = request
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest
            }
            
            return redirectedRequest
        }
        
        Alamofire.request(url, method: .delete, parameters: parameters,encoding: JSONEncoding.prettyPrinted , headers: headers).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func DeleteRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .delete, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func downloadFile(url: String, callback: ((String?) -> Void)?) {
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let file = directoryURL.appendingPathComponent(ORDER_NUMBER! + ".pdf" , isDirectory: false)
            return (file, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        Alamofire.download(url, to:destination)
            .downloadProgress { (progress) in
                print((String)(progress.fractionCompleted))
        }
        .responseData { (data) in
            
            //Get the local docs directory and append your local filename.
            var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
            
            docURL = docURL?.appendingPathComponent(ORDER_NUMBER! + ".pdf") as NSURL?
            
            //Lastly, write your file to the disk.
            do {
                try data.value?.write(to: docURL! as URL)//.writeToURL(docURL!, atomically: true)
            }
            catch {
                
            }
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let path = paths[0]
            
            let pathToFile = (path as NSString).appendingPathComponent(ORDER_NUMBER! + ".pdf")
            
            callback!(pathToFile)
            if FileManager.default.fileExists(atPath: pathToFile){
                print("file Exist")
                return
            }

            print(data)
            print("Your download has been completed")
            
        }
        
    }
    
    static func getSaveFileUrl(fileName: String) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL;
    }
    
    
    static func UploadImage(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                
                if let data = value as? Data {
                    multipartFormData.append(data, withName: "file", fileName: "file.jpeg", mimeType: "image/jpeg")
                    //  multipartFormData.append(data, withName: key, fileName: "file.jpg", mimeType: "image/jpg")
                    //  multipartFormData.append(data, withName: "", fileName: "filename.jpg", mimeType: "image/jpg")
                } else  {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            }
        }, to: url, method: .post,headers: [:])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            callback?(response, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }
    
    static func UploadParkingSpot(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        let token = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
        let headers:[String:String] = ["Authorization" : "Bearer "+token+""]
        print("Header: \(headers)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                
                if let data = value as? Data {
                    multipartFormData.append(data, withName: "spot_image", fileName: "filename.jpg", mimeType: "image/jpg")
                } else  {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            }
        }, to: url, method: .post,headers: headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            callback?(response, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }
    
}





