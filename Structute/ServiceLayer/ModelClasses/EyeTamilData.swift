//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class EyeTamilData : NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let valam = "valam"
        static let sri = "sri"
        static let event = "event"
        static let que = "que"
        static let world = "world"
        static let popular = "popular"
        static let tech = "tech"
        static let cinema = "cinema"
        static let sport = "sport"
        static let canadian = "canadian"

    }
    
    // MARK: Properties
    public var valam: [NewsData]?
    public var sri: [NewsData]?
    public var event: [NewsData]?
    public var que: [NewsData]?
    public var world: [NewsData]?
    public var popular: [NewsData]?
    public var tech: [NewsData]?
    public var cinema: [NewsData]?
    public var sport: [NewsData]?
    public var canadian: [NewsData]?


    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.valam].array { valam = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.sri].array { sri = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.event].array { event = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.que].array { que = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.world].array { world = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.popular].array { popular = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.tech].array { tech = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.cinema].array { cinema = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.sport].array { sport = items.map { NewsData(json: $0) } }
        if let items = json[SerializationKeys.canadian].array { canadian = items.map { NewsData(json: $0) } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = valam { dictionary[SerializationKeys.valam] = value.map { $0.dictionaryRepresentation() } }
        if let value = sri { dictionary[SerializationKeys.sri] = value.map { $0.dictionaryRepresentation() } }
        if let value = event { dictionary[SerializationKeys.event] = value.map { $0.dictionaryRepresentation() } }
        if let value = que { dictionary[SerializationKeys.que] = value.map { $0.dictionaryRepresentation() } }
        if let value = world { dictionary[SerializationKeys.world] = value.map { $0.dictionaryRepresentation() } }
        if let value = popular { dictionary[SerializationKeys.popular] = value.map { $0.dictionaryRepresentation() } }
        if let value = tech { dictionary[SerializationKeys.tech] = value.map { $0.dictionaryRepresentation() } }
        if let value = cinema { dictionary[SerializationKeys.cinema] = value.map { $0.dictionaryRepresentation() } }
        if let value = sport { dictionary[SerializationKeys.sport] = value.map { $0.dictionaryRepresentation() } }
        if let value = canadian { dictionary[SerializationKeys.canadian] = value.map { $0.dictionaryRepresentation() } }

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.valam = aDecoder.decodeObject(forKey: SerializationKeys.valam) as? [NewsData]
        self.sri = aDecoder.decodeObject(forKey: SerializationKeys.sri) as? [NewsData]
        self.event = aDecoder.decodeObject(forKey: SerializationKeys.event) as? [NewsData]
        self.que = aDecoder.decodeObject(forKey: SerializationKeys.que) as? [NewsData]
        self.world = aDecoder.decodeObject(forKey: SerializationKeys.world) as? [NewsData]
        self.popular = aDecoder.decodeObject(forKey: SerializationKeys.popular) as? [NewsData]
        self.tech = aDecoder.decodeObject(forKey: SerializationKeys.tech) as? [NewsData]
        self.cinema = aDecoder.decodeObject(forKey: SerializationKeys.cinema) as? [NewsData]
        self.sport = aDecoder.decodeObject(forKey: SerializationKeys.sport) as? [NewsData]
        self.canadian = aDecoder.decodeObject(forKey: SerializationKeys.canadian) as? [NewsData]

    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(valam, forKey: SerializationKeys.valam)
        aCoder.encode(sri, forKey: SerializationKeys.sri)
        aCoder.encode(event, forKey: SerializationKeys.event)
        aCoder.encode(que, forKey: SerializationKeys.que)
        aCoder.encode(world, forKey: SerializationKeys.world)
        aCoder.encode(popular, forKey: SerializationKeys.popular)
        aCoder.encode(tech, forKey: SerializationKeys.tech)
        aCoder.encode(cinema, forKey: SerializationKeys.cinema)
        aCoder.encode(sport, forKey: SerializationKeys.sport)
        aCoder.encode(canadian, forKey: SerializationKeys.canadian)
    }

}

    
