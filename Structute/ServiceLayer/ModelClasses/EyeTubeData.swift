//
//  BaseClass.swift
//
//  Created by Muqeem's Macbook on 25/02/2021
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class EyeTubeData : NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let featured_image_src = "featured_image_src"
        static let excerpt = "excerpt"
    }
    
    // MARK: Properties
    public var title:Title?
    public var featured_image_src:String?
    public var excerpt: Excerpt?
    
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        title = Title(json: json[SerializationKeys.title])
        featured_image_src = json[SerializationKeys.featured_image_src].string
        excerpt = Excerpt(json: json[SerializationKeys.excerpt])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = title { dictionary[SerializationKeys.title] = value.dictionaryRepresentation() }
        if let value = featured_image_src { dictionary[SerializationKeys.featured_image_src] = value }
        if let value = excerpt { dictionary[SerializationKeys.excerpt] = value.dictionaryRepresentation() }
        
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? Title
        self.featured_image_src = aDecoder.decodeObject(forKey: SerializationKeys.featured_image_src) as? String
        self.excerpt = aDecoder.decodeObject(forKey: SerializationKeys.excerpt) as? Excerpt
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: SerializationKeys.title)
        aCoder.encode(featured_image_src, forKey: SerializationKeys.featured_image_src)
        aCoder.encode(excerpt, forKey: SerializationKeys.excerpt)
    }
    
}
