//
//  NewsData.swift
//  Eye Tamil
//
//  Created by Muqeem's Macbook on 21/02/2021.
//

import Foundation
import SwiftyJSON

public final class NewsData : NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let body = "body"
        static let youtube = "youtube"
        static let imageUrl = "image_url"
    }
    
    // MARK: Properties
    public var title: String?
    public var body: String?
    public var youtube: String?
    public var imageUrl: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        title = json[SerializationKeys.title].string
        body = json[SerializationKeys.body].string
        youtube = json[SerializationKeys.youtube].string
        imageUrl = json[SerializationKeys.imageUrl].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = body { dictionary[SerializationKeys.body] = value }
        if let value = youtube { dictionary[SerializationKeys.youtube] = value }
        if let value = imageUrl { dictionary[SerializationKeys.imageUrl] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
        self.body = aDecoder.decodeObject(forKey: SerializationKeys.body) as? String
        self.youtube = aDecoder.decodeObject(forKey: SerializationKeys.youtube) as? String
        self.imageUrl = aDecoder.decodeObject(forKey: SerializationKeys.imageUrl) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: SerializationKeys.title)
        aCoder.encode(body, forKey: SerializationKeys.body)
        aCoder.encode(youtube, forKey: SerializationKeys.youtube)
        aCoder.encode(imageUrl, forKey: SerializationKeys.imageUrl)
    }
    
}
